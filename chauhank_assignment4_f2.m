%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOAD DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
% data format - each line doc_id, word_id, word_count

% subtract a-1 from indices which are in stop list. then proceed as normal

fileID = fopen('train.data');
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('test.data');
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('train.label');
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

fileID = fopen('test.label');
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

vocab = importdata('vocabulary.txt');

stopwords = importdata('stoplist.txt');

toc
%%%%%%%%%%%%%%%%%%%%%%%%%% PRUNE DATASETS %%%%%%%%%%%%%%%%%%%%%%%%%%

% get word id corresponding to each stop word
[~, stop_word_ids] = ismember(stopwords, vocab);

% do not consider words that are in stoplist but not vocab
stop_word_ids = unique(stop_word_ids(stop_word_ids~=0));

% remove stop words from vocab first
vocab_words_idx = setdiff(1:size(vocab,1), int32(stop_word_ids));
vocab = vocab(vocab_words_idx, :);

foo = (unique(X_train(:,1)));
% DOC ID 5957 IS A PAIN

% remove stop words from train and test sets
idx = ~ismember(X_train(:,2), stop_word_ids);
X_train = X_train(idx,:);
bar = (unique(X_train(:,1)));

res = (~ismember(foo, bar));

idx = find(res);

idx = ~ismember(X_test(:,2), stop_word_ids);
X_test = X_test(idx, :);


%%%%%%%%%%%%%%%%%%%%%%%%%% TRAIN THE CLASSIFIER %%%%%%%%%%%%%%%%%%%%%%%%%%

% number of different classes
num_bins = numel(unique(y_train));

% number of documents of each class
num_docs_of_class = histcounts(y_train, num_bins);

% log probability of seeing each class
class_log_probs = log(num_docs_of_class/sum(num_docs_of_class));

% parameter alpha for Dirichlet prior
alpha = 1 + 1/size(vocab,1);

% word given class log probabilities for all classes
all_classes_word_class_log_prob = zeros(20, num_words_vocab);

for class_id = 1:20
    
    % doc_id's that belong to this class_id
    this_class_docs = find(y_train==class_id);
    
    % documents and words that are of this class_id
    this_class_docs_words = X_train(y_train==class_id, :);
    
     % sort just the word_id column of docs-words matrix
    [~, idx] = sort(this_class_docs_words(:, 2));

    % sort the entire matrix using the sort indices (by word_id)
    this_class_docs_words = this_class_docs_words(idx, :);

    % get indices where each word starts
    [~, words_start_idx, ~] = unique(this_class_docs_words(:, 2));
        
    for word_idx = 1:numel(words_start_idx)-1
        probs_word_given_class(1, this_class_docs_words(words_start_idx(word_idx), 2)) = ...
            sum(this_class_docs_words(words_start_idx(word_idx):words_start_idx(word_idx+1)-1, 3));
    end
    probs_word_given_class(1,this_class_docs_words(end,2)) = sum(this_class_docs_words(words_start_idx(end):end, 3));
    
    % add the alpha factor
    probs_word_given_class = probs_word_given_class + (alpha-1);
    
    % divide by sum to get probability
    probs_word_given_class = probs_word_given_class/sum(probs_word_given_class);
    
    % append the result to all classes probability of word given class matrix
    all_classes_word_class_log_prob(class_id, :) = probs_word_given_class;
    
end

