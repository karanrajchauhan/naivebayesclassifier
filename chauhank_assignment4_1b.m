%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOAD DATASETS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
% format - each line doc_id, word_id, word_count

fileID = fopen('train.data');
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('test.data');
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('train.label');
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

fileID = fopen('test.label');
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

vocab = importdata('vocabulary.txt');

% total number of unique words
num_words_vocab = numel(unique(vocab));

toc
%%%%%%%%%%%%%%%%%%%%%%%%%% TRAIN THE CLASSIFIER %%%%%%%%%%%%%%%%%%%%%%%%%%
tic
% number of different classes
num_classes = numel(unique(y_train));

% number of documents of each class
num_docs_in_classes = histcounts(y_train, num_classes);

% log probability of seeing each class
class_log_probs = log(num_docs_in_classes/sum(num_docs_in_classes));

%  NOTE: loop runs faster without initialization
% % initialize matrices to be used in for loop
% class_X = zeros(numel(X_train(:,1)), 3);
% class_uniq_words = zeros(num_words_vocab, 1);
% class_word_freqs = zeros(num_words_vocab, 1);

% log prob of word given class for all words for all classes
probs_word_class_all_classes = zeros(20, num_words_vocab);

for class_id = 1:20
    
    % doc ids of this class
    class_doc_ids = find(y_train == class_id);
    
    % data for current class
    class_X = X_train(ismember(X_train(:,1), class_doc_ids), :);
    
    % the unique words that appear in current class
    class_uniq_words = unique(class_X(:,2));

    % number of times each word appears in current class
    class_word_freqs = accumarray(class_X(:, 2), class_X(:, 3), [], @sum);
    
    % remove 0 frequencies
    class_word_freqs = class_word_freqs(class_word_freqs~=0);

    % log prob = log(freq/total_words) for current class
    probs_word_class_all_classes(class_id, class_uniq_words) = (class_word_freqs/sum(class_word_freqs));
    
end

% number of -INF's in log beta matrix
num_zero_params = sum(probs_word_class_all_classes(:) == 0);

% % convert from prob to log prob
% log_probs_word_class_all_classes = log(log_probs_word_class_all_classes);

toc
%%%%%%%%%%%%%%%%%%%%%%%%%% CLASS PROBABILITY PMF %%%%%%%%%%%%%%%%%%%%%%%%%%

% % x axis data points
% classes = 1:20;
% 
% % pmf
% stem(classes, num_docs_in_classes/sum(num_docs_in_classes));
% 
% % axes labels, graph title, y axis range
% ax = gca;
% ax.Title.String = 'PRIOR CLASS PROBABILTIES';
% ax.XLabel.String = 'c';
% ax.YLabel.String = 'P( Y=c )';
% ax.YLim = [0 0.1];


%%%%%%%%%%%%%%%%%%%%%%%%%%% TEST THE CLASSIFIER %%%%%%%%%%%%%%%%%%%%%%%%%%%
tic

% uncomment for part c
words_test_not_train = setdiff(unique(X_test(:,2)), unique(X_train(:,2)));
X_test = X_test(~ismember(X_test(:, 2), words_test_not_train), :);

test_doc_ids = unique(X_test(:,1));

% initialize to 0's
doc_word_freqs = zeros(num_words_vocab, 1);
doc_class_probs = zeros(20,1);

% predicted class for each doc, prediction log probability
predictions = zeros(numel(test_doc_ids), 2);

for doc_id = 1:numel(test_doc_ids)
    
    % get all the test data for current doc id
    doc_X = X_test(X_test(:,1)==doc_id, :);
    
    % how many times each word occurs in this doc
    doc_word_freqs(doc_X(:,2), 1) = doc_X(:,3);
    
%     % results in too many nan's
%     doc_class_probs = (log_probs_word_class_all_classes * doc_word_freqs);

    prob_exp_freq = probs_word_class_all_classes .^ repmat(doc_word_freqs', 20, 1);
    doc_class_probs = sum(log(prob_exp_freq), 2);
    
%     % find each product and then replace nan with 0
%     freq_log_prob_prods = probs_word_class_all_classes .* repmat(doc_word_freqs', 20, 1);
%     freq_log_prob_prods(isnan(freq_log_prob_prods)) = 0;
%     
%     % probability of doc being of class i
%     doc_class_probs = sum(freq_log_prob_prods, 2);
    
    % predicted class will be the one with max probability
    [predictions(doc_id, 2), predictions(doc_id, 1)] = max(doc_class_probs);
    
    % check if more than one class with same max probability. If so, choose class with max prob
    num_same_max = doc_class_probs(:) == predictions(doc_id, 2);
    if sum(num_same_max) > 1
        [~, predictions(doc_id, 1)] = max(class_log_probs(num_same_max));
    end
    
end

% how many documents have P(y=c) = 0 for all classes
num_zero_probs = sum(predictions(:, 2)==(-Inf));

% correct classification rate
ccr = sum(predictions(:,1) == y_test)/numel(y_test);

% confusion matrix
cmat = confusionmat(y_test, int32(predictions(:,1)));

toc




%%%%%%%%%%%%%%%%%%%%%%%%%%% TEST THE CLASSIFIER %%%%%%%%%%%%%%%%%%%%%%%%%%%
% tic
% 
% % sort according to word_ids
% [~, sorted_test_idx] = sort(X_test(:, 1));
% X_test = X_test(sorted_test_idx, :);
% 
% % get indices at which each unique word starts
% [~, docs_start_indices, ~] = unique(X_test(:, 1));
% 
% % initialize to save agony
% this_doc_word_freqs = zeros(1, num_total_words);
% 
% % predicted class for each doc, prediction log probability
% predictions = zeros(numel(docs_start_indices), 2);
% 
% this_class_res = zeros(20,1);
% 
% for indexer = 1:numel(docs_start_indices)-1
% 
%     % data for this doc id
%     this_doc_X = X_test(docs_start_indices(indexer):docs_start_indices(indexer+1)-1, :);
%     
%     % calculate frequencies of words in current doc id
%     this_doc_word_freqs(1, this_doc_X(:,2)) = this_doc_X(:,3);
%     
%     % word frequencies times log probs + log prob of class itself
%     this_class_res = (all_classes_word_class_log_prob(:, this_doc_X(:,2)) * double(this_doc_X(:,3))) + (class_log_probs);
%         
%     % predicted class will be the one with max probability
%     [predictions(this_doc_X(1,1), 2), predictions(this_doc_X(1,1), 1)] = max(this_class_res);
%     
%     % check if more than one class with same max probability. If so, choose class with max prob
%     num_same_max = this_class_res(:) == predictions(this_doc_X(1,1), 2);
%     if sum(num_same_max) > 1
%         [~, predictions(this_doc_X(1,1), 1)] = max(class_log_probs(num_same_max));
%     end
%         
% end
% 
% % do all the calculations for the last doc_id, since it wasn't done in loop
% 
% last_doc_X = X_test(docs_start_indices(end):end, :);
% 
% last_doc_word_freqs(1, last_doc_X(:,2)) = last_doc_X(:,3);
% 
% this_class_res = (all_classes_word_class_log_prob(:, last_doc_X(:,2)) * double(last_doc_X(:,3))) + (class_log_probs);
% 
% [predictions(last_doc_X(1,1), 2), predictions(last_doc_X(1,1), 1)] = max(this_class_res);
% 
% num_same_max = this_class_res(:) == predictions(last_doc_X(1,1), 2);
% if sum(num_same_max) > 1
%     [~, predictions(last_doc_X(1,1), 1)] = max(class_log_probs(num_same_max));
% end
% 
% 
% 
% 
% 
% toc





