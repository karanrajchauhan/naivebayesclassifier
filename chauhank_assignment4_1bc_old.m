%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOAD DATASETS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% format - each line doc_id, word_id, word_count
tic
fileID = fopen('train.data');
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('test.data');
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('train.label');
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

fileID = fopen('test.label');
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

vocab = importdata('vocabulary.txt');

num_total_words = numel(unique(vocab));
num_total_docs = numel(unique(X_train(:,1)));
toc

%%%%%%%%%%%%%%%%%%%%%%%%%%% CLASS PROBABILITIES %%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
% number of documents in each class
num_docs_this_class = zeros(20, 1);

% each row is a vector of probability of seeing each word, given that class (row num)
all_classes_word_class_log_prob = zeros(20, num_total_words);

% initialize vector probability of seeing each word, given current class
probs_word_given_class = zeros(1, num_total_words);

for class_id = 1:20
    
    % doc_id's that belong to this class_id
    this_class_docs = find(y_train==class_id);
    
    % number of documents of class class_id
    num_docs_this_class(class_id, 1) = numel(this_class_docs);
    
    % documents and words that are of this class_id
    this_class_docs_words = X_train(ismember(X_train(:,1), this_class_docs), :);
    
     % sort just the word_id column of docs-words matrix
    [~, idx] = sort(this_class_docs_words(:, 2));

    % sort the entire matrix using the sort indices (by word_id)
    this_class_docs_words = this_class_docs_words(idx, :);

    % get indices where each word starts
    [~, words_start_idx, ~] = unique(this_class_docs_words(:, 2));
        
    for word_idx = 1:numel(words_start_idx)-1
        probs_word_given_class(1, this_class_docs_words(words_start_idx(word_idx), 2)) = ...
            sum(this_class_docs_words(words_start_idx(word_idx):words_start_idx(word_idx+1)-1, 3));
    end
    probs_word_given_class(1,this_class_docs_words(end,2)) = sum(this_class_docs_words(words_start_idx(end):end, 3));
    
    % divide by sum to get probability
    probs_word_given_class = probs_word_given_class/sum(probs_word_given_class);
    
    % append the result to all classes probability of word given class matrix
    all_classes_word_class_log_prob(class_id, :) = probs_word_given_class;
    
end



% divide by total number of docs
class_log_probs = log(num_docs_this_class/sum(num_docs_this_class, 1));

% number of 0's in beta matrix
disp(sum(sum(all_classes_word_class_log_prob(:) == 0)));

% convert probabilties to log probabilities
all_classes_word_class_log_prob = log(all_classes_word_class_log_prob);

toc
%%%%%%%%%%%%%%%%%%%%%%%%%% CLASS PROBABILITY PMF %%%%%%%%%%%%%%%%%%%%%%%%%%

% % x axis data points
% classes = 1:20;
% 
% % pmf
% stem(classes, class_probs);
% 
% % axes labels, graph title, y axis range
% ax = gca;
% ax.Title.String = 'PRIOR CLASS PROBABILTIES';
% ax.XLabel.String = 'c';
% ax.YLabel.String = 'P( Y=c )';
% ax.YLim = [0 0.1];
% 

% changed for part c
% number of 0's in the sparse matrix
% foo = all_classes_prob_word_class(:, 1:53975);
% disp(sum(foo(:)~=0));


%%%%%%%%%%%%%%%%%%%%%%%%%% TEST CLASSIFIER  %%%%%%%%%%%%%%%%%%%%%%%%%%
tic

% % uncomment the following line for part c
% X_test = X_test(~ismember(X_test(:, 2), words_test_not_train), :);

% sort according to word_ids
[~, sorted_test_idx] = sort(X_test(:, 1));
X_test = X_test(sorted_test_idx, :);

% get indices at which each unique word starts
[~, docs_start_indices, ~] = unique(X_test(:, 1));

% initialize to save agony
this_doc_word_freqs = zeros(1, num_total_words);

% predicted class for each doc, prediction log probability
predictions = zeros(numel(docs_start_indices), 2);

this_class_res = zeros(20,1);

for indexer = 1:numel(docs_start_indices)-1

    % data for this doc id
    this_doc_X = X_test(docs_start_indices(indexer):docs_start_indices(indexer+1)-1, :);
    
    % calculate frequencies of words in current doc id
    this_doc_word_freqs(1, this_doc_X(:,2)) = this_doc_X(:,3);
    
    % word frequencies times log probs + log prob of class itself
    this_class_res = (all_classes_word_class_log_prob * double(this_doc_word_freqs')) + (class_log_probs);
        
    % predicted class will be the one with max probability
    [predictions(this_doc_X(1,1), 2), predictions(this_doc_X(1,1), 1)] = max(this_class_res);
    
    % check if more than one class with same max probability. If so, choose class with max prob
    num_same_max = this_class_res(:) == predictions(this_doc_X(1,1), 2);
    if sum(num_same_max) > 1
        [~, predictions(this_doc_X(1,1), 1)] = max(class_log_probs(num_same_max));
    end
        
end

% do all the calculations for the last doc_id, since it wasn't done in loop

last_doc_X = X_test(docs_start_indices(end):end, :);

last_doc_word_freqs(1, last_doc_X(:,2)) = last_doc_X(:,3);

this_class_res = (all_classes_word_class_log_prob * double(last_doc_word_freqs')) + (class_log_probs);

[predictions(last_doc_X(1,1), 2), predictions(last_doc_X(1,1), 1)] = max(this_class_res);

num_same_max = this_class_res(:) == predictions(last_doc_X(1,1), 2);
if sum(num_same_max) > 1
    [~, predictions(last_doc_X(1,1), 1)] = max(class_log_probs(num_same_max));
end

toc


%%%%%%%%%%%%%%%%%%%%%%% QUESTIONS ABOUT PRED MATRIX %%%%%%%%%%%%%%%%%%%%%%%

% how many documents have P(y=c) = 0 for all classes
num_zero_probs = sum(predictions(:, 2)==(-Inf));

% correct classification rate
ccr = sum(predictions(:,1) == y_test)/numel(y_test);

% confusion matrix
cmat = confusionmat(y_test, int32(predictions(:,1)));








