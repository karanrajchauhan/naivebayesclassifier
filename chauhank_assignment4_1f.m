%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOAD DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
% data format - each line doc_id, word_id, word_count

% subtract a-1 from indices which are in stop list. then proceed as normal

fileID = fopen('train.data');
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('test.data');
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('train.label');
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

fileID = fopen('test.label');
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

vocab = importdata('vocabulary.txt');

stopwords = importdata('stoplist.txt');

toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%% ALTER DATASETS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
% NOTE: setting freq to 0 also works but create a new column corresponding to old id's

% get word id corresponding to each stop word
[~, stop_word_ids] = ismember(stopwords, vocab);

% do not consider words that are in stoplist but not vocab
stop_word_ids = unique(stop_word_ids(stop_word_ids~=0));

% removing stop words
idx = ~ismember(X_train(:,2), stop_word_ids);
X_train = X_train(idx,:);

idx = ~ismember(X_test(:,2), stop_word_ids);
X_test = X_test(idx, :);

% combining train and test
X = [X_train; X_test];


%%%%%%%%%%%%%%%%%%%%%%%%%%%% GET NEW STATS %%%%%%%%%%%%%%%%%%%%%%%%%%%%

% getting unique words
uniq_train_words = unique(X_train(:,2));
uniq_test_words = unique(X_test(:,2));

% getting number of unique words
num_uniq_train = numel(uniq_train_words);
num_uniq_test = numel(uniq_test_words);
num_uniq_total = numel(unique(X(:,2)));
num_uniq_test_not_train = numel(setdiff(uniq_test_words, uniq_train_words));

% getting lengths of documents
train_doc_lens = accumarray(X_train(:,1), X_train(:,3), [], @sum);
test_doc_lens = accumarray(X_test(:,1), X_test(:,3), [], @sum);

% mean length
train_doc_len_mean = mean(train_doc_lens);
test_doc_len_mean = mean(test_doc_lens);

% std length
train_doc_len_std = std(train_doc_lens);
test_doc_len_std = std(test_doc_lens);


%%%%%%%%%%%%%%%%%%%%%%%%%%%% WORD FREQUENCIES %%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sort by word id
[~, idx_word] = sort(X(:,2));
X = X(idx_word, :);

% get frequencies of words
word_freqs = accumarray(X(:,2), X(:,3), [], @sum);

% get the frequencies sorted descending, and also get corresponding word id's
[highest_freqs, highest_freq_word_ids] = sort(word_freqs, 'descend');

% get top 10 frequent words and their frequencies
top_ten_words = vocab(highest_freq_word_ids(1:10));
top_ten_freqs = highest_freqs(1:10);

% get lowest frequency (after 0) and the words with that frequency
lowest_freq = min(highest_freqs(highest_freqs~=0));
lowest_words = vocab(highest_freq_word_ids(highest_freqs==lowest_freq));

% from lowest freq words, get the ones that begin with aero
aero_words = lowest_words(startsWith(lowest_words, 'aero'));

toc