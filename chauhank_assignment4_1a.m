%%%%%%%%%%%%%%%%%%%%%% LOAD TRAIN AND TEST DATASETS %%%%%%%%%%%%%%%%%%%%%%%
tic
% load train and test datasets (format - doc_id, word_id, word_count)
fileID = fopen('train.data');
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('test.data');
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

vocab = importdata('vocabulary.txt');

toc
%%%%%%%%%%%%%%%%%%%%%% GET UNIQUE WORDS IN DATASETS %%%%%%%%%%%%%%%%%%%%%%%
tic
% get unique words in train and test sets
uniq_train_words = unique(X_train(:, 2));
uniq_test_words = unique(X_test(:, 2));

% number of unique words
num_uniq_train = numel(uniq_train_words);
num_uniq_test = numel(uniq_test_words);
num_uniq_total = numel(union(uniq_train_words, uniq_test_words));
num_uniq_test_not_train = numel(setdiff(uniq_test_words, uniq_train_words));

%%%%%%%%%%%%%%%%%%%%%% GET DOCUMENT LENGHTS %%%%%%%%%%%%%%%%%%%%%%%

% getting lengths of documents
train_doc_lens = accumarray(X_train(:,1), X_train(:,3), [], @sum);
test_doc_lens = accumarray(X_test(:,1), X_test(:,3), [], @sum);

% mean length
train_doc_len_mean = mean(train_doc_lens);
test_doc_len_mean = mean(test_doc_lens);

% std length
train_doc_len_std = std(train_doc_lens);
test_doc_len_std = std(test_doc_lens);


%%%%%%%%%%%%%%%%%%%%%% GET WORD FREQUENCIES %%%%%%%%%%%%%%%%%%%%%%%

% combine train and test
X = [X_train; X_test];

% get frequencies of words
word_freqs = accumarray(X(:,2), X(:,3), [], @sum);

% get the frequencies sorted descending, and also get corresponding word id's
[highest_freqs, highest_freq_word_ids] = sort(word_freqs, 'descend');

% get top 10 frequent words and their frequencies
top_ten_words = vocab(highest_freq_word_ids(1:10));
top_ten_freqs = highest_freqs(1:10);

% get lowest frequency and the words with that frequency
lowest_freq = min(highest_freqs);
lowest_words = vocab(highest_freq_word_ids(highest_freqs==lowest_freq));

% from lowest freq words, get the ones that begin with od
od_words = lowest_words(startsWith(lowest_words, 'od'));
toc