%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOAD DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% format - each line doc_id, word_id, word_count

fileID = fopen('train.data');
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('test.data');
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('train.label');
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

fileID = fopen('test.label');
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

vocab = importdata('vocabulary.txt');

num_words_vocab = numel(unique(vocab));


%%%%%%%%%%%%%%%%%%%%%%%%%% TRAIN FOR EACH ALPHA %%%%%%%%%%%%%%%%%%%%%%%%%%%

% number of documents in each class
num_docs_in_classes = zeros(20, 1);

% initialize vector probability of seeing each word, given current class
probs_word_given_class = zeros(1, num_words_vocab);

% number of distinct alpha values to test for
num_alpha_vals = 14;

% log probabilities of word given class for each class
all_alphas_log_probs_class_words = zeros(20, num_words_vocab, num_alpha_vals);

for class_id = 1:20

    % doc_id's that belong to this class_id
    this_class_docs = find(y_train==class_id);

    % number of documents of class class_id
    num_docs_in_classes(class_id, 1) = numel(this_class_docs);

    % documents and words that are of this class_id
    this_class_docs_words = X_train(ismember(X_train(:,1), this_class_docs), :);

     % sort just the word_id column of docs-words matrix
    [~, idx] = sort(this_class_docs_words(:, 2));

    % sort the entire matrix using the sort indices (by word_id)
    this_class_docs_words = this_class_docs_words(idx, :);

    % get indices where each word starts
    [~, words_start_idx, ~] = unique(this_class_docs_words(:, 2));

    for word_idx = 1:numel(words_start_idx)-1
        probs_word_given_class(1, this_class_docs_words(words_start_idx(word_idx), 2)) = ...
            sum(this_class_docs_words(words_start_idx(word_idx):words_start_idx(word_idx+1)-1, 3));
    end
    probs_word_given_class(1,this_class_docs_words(end,2)) = sum(this_class_docs_words(words_start_idx(end):end, 3));

    
    alpha_val_idx = 1;
    
    for alpha = 1 + 10.^(-5:0.5:1.5)
    
        % add the alpha factor
        probs_word_given_class_alpha = probs_word_given_class + (alpha-1);

        % divide by sum to get probability
        probs_word_given_class_alpha = probs_word_given_class_alpha/sum(probs_word_given_class_alpha);

        % append the result to all classes probability of word given class matrix
        all_alphas_log_probs_class_words(class_id, :, alpha_val_idx) = probs_word_given_class_alpha;
        
        % increment alpha value index
        alpha_val_idx = alpha_val_idx + 1;
        
    end

end

% divide by total number of docs
class_log_probs = log(num_docs_in_classes/sum(num_docs_in_classes, 1));

% convert probabilties to log probabilities
all_alphas_log_probs_class_words = log(all_alphas_log_probs_class_words);


%%%%%%%%%%%%%%%%%%%%%%%%%% TEST FOR EACH ALPHA  %%%%%%%%%%%%%%%%%%%%%%%%%%%

% sort according to word_ids
[~, sorted_test_idx] = sort(X_test(:, 1));
X_test = X_test(sorted_test_idx, :);

% get indices at which each unique word starts
[~, docs_start_indices, ~] = unique(X_test(:, 1));

% number of predictions to make
num_test_docs = numel(docs_start_indices);

% predicted class for each doc, prediction log probability
predictions = zeros(num_test_docs, 2, num_alpha_vals);

% initialize this class results
this_doc_class_probs = zeros(20,1);

for indexer = 1:num_test_docs-1

    % data for this doc id
    this_doc_X = X_test(docs_start_indices(indexer):docs_start_indices(indexer+1)-1, :);
    
    alpha_val_idx = 1;
    for alpha = 1 + 10.^(-5:0.5:1.5)

        % word frequencies times log probs + log prob of class itself
        this_doc_class_probs = (all_alphas_log_probs_class_words(:, this_doc_X(:,2), alpha_val_idx) * double(this_doc_X(:,3))) + (class_log_probs);

        % predicted class will be the one with max probability. save max prob, class of max prob
        [predictions(this_doc_X(1,1), 2, alpha_val_idx), predictions(this_doc_X(1,1), 1, alpha_val_idx)] = max(this_doc_class_probs);

        % check if more than one class with same max probability. If so, choose class with max prob
        num_same_max = this_doc_class_probs(:) == predictions(this_doc_X(1,1), 2, alpha_val_idx);
        if sum(num_same_max) > 1
            [~, predictions(this_doc_X(1,1), 1, alpha_val_idx)] = max(class_log_probs(num_same_max));
        end
        
        alpha_val_idx = alpha_val_idx + 1;

    end
        
end

% do all the calculations for the last doc_id, since it wasn't done in loop

last_doc_X = X_test(docs_start_indices(end):end, :);

alpha_val_idx = 1;
for alpha = 1 + 10.^(-5:0.5:1.5)
    
    this_doc_class_probs = (all_alphas_log_probs_class_words(:, last_doc_X(:,2), alpha_val_idx) * double(last_doc_X(:,3))) + (class_log_probs);

    [predictions(last_doc_X(1,1), 2, alpha_val_idx), predictions(last_doc_X(1,1), 1, alpha_val_idx)] = max(this_doc_class_probs);

    num_same_max = this_doc_class_probs(:) == predictions(last_doc_X(1,1), 2, alpha_val_idx);
    if sum(num_same_max) > 1
        [~, predictions(last_doc_X(1,1), 1, alpha_val_idx)] = max(class_log_probs(num_same_max));
    end
    
    alpha_val_idx = alpha_val_idx + 1;
end

% find ccr for each value of alpha

ccrs_alphas = zeros(num_alpha_vals, 1);

for ccr_for_alpha_idx = 1:num_alpha_vals
    
    ccrs_alphas(ccr_for_alpha_idx, 1) = sum(predictions(:, 1, ccr_for_alpha_idx) == y_test)/num_test_docs;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLOT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% alpha-1 on a log scale
x_pts = -5:0.5:1.5;

% log(a-1) vs ccr graph
plot(x_pts, ccrs_alphas, 'b.-')

% axes labels, graph title, y axis range
ax = gca;
ax.Title.String = 'CCRs vs log(a-1)';
ax.XLabel.String = 'log(a-1)';
ax.YLabel.String = 'CCR';


