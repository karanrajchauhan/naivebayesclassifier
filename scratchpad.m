% a = randsample(5, 10, true);
% a = sort(a);
% disp(a);
% 
% [b, idx] = max(a);
% disp(b);
% disp(idx);

% % 
% [foo, ia, ic] = unique(a);
% disp(foo);
% disp(ia);
% disp(ic);

% a = zeros(10,1);
% a(2,1) = 1;
% a(6,1) = 1;
% disp(a);
% 
% b = find(a);
% disp(b);

% x = [1 2 3; 1 3 5; 2 3 4; 2 4 6];
% 
% [uv,~,idx] = unique(x(:,1));
% 
% nu = numel(uv);
% 
% x_sum = zeros(nu,1);
% 
% for ii = 1:nu
%   x_sum(ii,1) = sum(x(idx==ii,3));
% end

% foo = zeros(5);
% foo(2,3) = 213;
% foo(4,1) = 21;
% foo(2,1) = 54;
% 
% disp(foo);
% disp(sum(foo(:)==0));

% a = zeros(5,1);
% a(1,1) = 2;
% a(3,1) = 2;
% 
% [val, idx] = max(a);
% ans = idx;
% 
% if sum((a(:)==val)) > 1
%     ans = 
% end

% y = int32(1:20);
% 
%  p = perms(y);
%  [m,n] = size(p);
%  z = zeros(m,2*n);
%  z(:,1:2:end-1) = repmat(x,m,1);
%  z(:,2:2:end) = p;
% 
% a = zeros(20,20);
% disp(size(a) == [20 20]);
% 
% plot(1:10, 2:20, 'r.-')
% legend('myline')
% 


% data format - each line doc_id, word_id, word_count

fileID = fopen('train.data');
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('test.data');
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('train.label');
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

fileID = fopen('test.label');
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

vocab = importdata('vocabulary.txt');

stopwords = importdata('stoplist.txt');

A = randi(10,3);
disp(A);
doc_lens = accumarray(A(:,1), A(:,3), [], @sum);
% disp(doc_lens);

b = randi(2,3);
disp(b);

c = [A; b]
