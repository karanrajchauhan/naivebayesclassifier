%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LOAD DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fileID = fopen('train.data');
X_train = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('test.data');
X_test = cell2mat(textscan(fileID, '%d %d %d'));
fclose(fileID);

fileID = fopen('train.label');
y_train = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

fileID = fopen('test.label');
y_test = cell2mat(textscan(fileID, '%d'));
fclose(fileID);

vocab = importdata('vocabulary.txt');

num_words_vocab = numel(unique(vocab));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GET ALPHA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% parameter alpha for Dirichlet prior
alpha = 1 + 1/num_words_vocab;

%%%%%%%%%%%%%%%%%%%%%%%%%%% CLASS PROBABILITIES %%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
% number of documents in each class
num_docs_this_class = zeros(20, 1);

% initialize vector probability of seeing each word, given current class
probs_word_given_class = zeros(1, num_words_vocab);

all_classes_word_class_log_prob = zeros(20, num_words_vocab);

for class_id = 1:20
    
    % doc_id's that belong to this class_id
    this_class_docs = find(y_train==class_id);
    
    % number of documents of class class_id
    num_docs_this_class(class_id, 1) = numel(this_class_docs);
    
    % documents and words that are of this class_id
    this_class_docs_words = X_train(ismember(X_train(:,1), this_class_docs), :);
    
     % sort just the word_id column of docs-words matrix
    [~, idx] = sort(this_class_docs_words(:, 2));

    % sort the entire matrix using the sort indices (by word_id)
    this_class_docs_words = this_class_docs_words(idx, :);

    % get indices where each word starts
    [~, words_start_idx, ~] = unique(this_class_docs_words(:, 2));
        
    for word_idx = 1:numel(words_start_idx)-1
        probs_word_given_class(1, this_class_docs_words(words_start_idx(word_idx), 2)) = ...
            sum(this_class_docs_words(words_start_idx(word_idx):words_start_idx(word_idx+1)-1, 3));
    end
    probs_word_given_class(1,this_class_docs_words(end,2)) = sum(this_class_docs_words(words_start_idx(end):end, 3));
    
    % add the alpha factor
    probs_word_given_class = probs_word_given_class + (alpha-1);
    
    % divide by sum to get probability
    probs_word_given_class = probs_word_given_class/sum(probs_word_given_class);
    
    % append the result to all classes probability of word given class matrix
    all_classes_word_class_log_prob(class_id, :) = probs_word_given_class;
    
end

% divide by total number of docs
class_log_probs = log(num_docs_this_class/sum(num_docs_this_class, 1));

% convert probabilties to log probabilities
all_classes_word_class_log_prob = log(all_classes_word_class_log_prob);
toc

%%%%%%%%%%%%%%%%%%%%%%%%%% TEST THE CLASSIFIER  %%%%%%%%%%%%%%%%%%%%%%%%%%
tic

% sort according to word_ids
[~, sorted_test_idx] = sort(X_test(:, 1));
X_test = X_test(sorted_test_idx, :);

% get indices at which each unique word starts
[~, docs_start_indices, ~] = unique(X_test(:, 1));

% initialize to save agony
this_doc_word_freqs = zeros(1, num_words_vocab);

num_test_docs = numel(docs_start_indices);

% predicted class for each doc, prediction log probability
predictions = zeros(num_test_docs, 2);

% initialize this class results
this_class_res = zeros(20,1);

for indexer = 1:num_test_docs-1

    % data for this doc id
    this_doc_X = X_test(docs_start_indices(indexer):docs_start_indices(indexer+1)-1, :);
    
    % calculate frequencies of words in current doc id
    this_doc_word_freqs(1, this_doc_X(:,2)) = this_doc_X(:,3);
    
    % word frequencies times log probs + log prob of class itself
    this_class_res = (all_classes_word_class_log_prob(:, this_doc_X(:,2)) * double(this_doc_X(:,3))) + (class_log_probs);
        
    % predicted class will be the one with max probability
    [predictions(this_doc_X(1,1), 2), predictions(this_doc_X(1,1), 1)] = max(this_class_res);
    
    % check if more than one class with same max probability. If so, choose class with max prob
    num_same_max = this_class_res(:) == predictions(this_doc_X(1,1), 2);
    if sum(num_same_max) > 1
        [~, predictions(this_doc_X(1,1), 1)] = max(class_log_probs(num_same_max));
    end
        
end

% do all the calculations for the last doc_id, since it wasn't done in loop

last_doc_X = X_test(docs_start_indices(end):end, :);

last_doc_word_freqs(1, last_doc_X(:,2)) = last_doc_X(:,3);

this_class_res = (all_classes_word_class_log_prob(:, last_doc_X(:,2)) * double(last_doc_X(:,3))) + (class_log_probs);

[predictions(last_doc_X(1,1), 2), predictions(last_doc_X(1,1), 1)] = max(this_class_res);

num_same_max = this_class_res(:) == predictions(last_doc_X(1,1), 2);
if sum(num_same_max) > 1
    [~, predictions(last_doc_X(1,1), 1)] = max(class_log_probs(num_same_max));
end

toc


%%%%%%%%%%%%%%%%%%%%%%% QUESTIONS ABOUT PRED MATRIX %%%%%%%%%%%%%%%%%%%%%%%
tic
% how many documents have P(y=c) = 0 for all classes
num_zero_probs = sum(predictions(:, 2)==(-Inf));

% correct classification rate
ccr = sum(predictions(:,1) == y_test)/num_test_docs;

% confusion matrix
confusion_mat = confusionmat(y_test, int32(predictions(:,1)));

% degree of confusion between all classes
all_classes_d_conf = zeros(20, 20);

for class_a = 1:20
    
    for class_b = class_a+1:20
        
        % calculate d(a,b)
        d_conf = (confusion_mat(class_a, class_b) + confusion_mat(class_b, class_a))/(2*num_test_docs);
        
        % save the score at indices (a,b) and (b,a)
        all_classes_d_conf(class_a, class_b) = d_conf;
        all_classes_d_conf(class_b, class_a) = d_conf;
    end
    
end

% sort descending
[~, sorted_index] = sort(all_classes_d_conf(:), 'descend');

most_conf = zeros(5, 2);

[most_conf(1, 1), most_conf(1, 2)] = ind2sub(size(all_classes_d_conf), sorted_index(1));
[most_conf(2, 1), most_conf(2, 2)] = ind2sub(size(all_classes_d_conf), sorted_index(3));
[most_conf(3, 1), most_conf(3, 2)] = ind2sub(size(all_classes_d_conf), sorted_index(5));
[most_conf(4, 1), most_conf(4, 2)] = ind2sub(size(all_classes_d_conf), sorted_index(7));
[most_conf(5, 1), most_conf(5, 2)] = ind2sub(size(all_classes_d_conf), sorted_index(9));
toc
